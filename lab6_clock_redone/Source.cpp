#include "library.h"

int month_length(const int year, const int month)
{
	if ((month<8) && (month % 2 == 0))
	{      
		if (month == 2)
		{
			if ((year % 400 == 0) || (year % 4 == 0 && year % 100 != 0))
			{
				return 29;
			}
			else
				return 28; 
		}
		else
			return 30;
	}
	else if (month < 8)
	{
		return 31;
	}
	else if (month % 2 == 0)
	{
		return 31;
	}
	else
		return 30;
}
int day_of_year(const int year, const int month, const int day)
{
	if (month > 1)
		return month_length(year, month - 1) + day_of_year(year, month - 1, day);
	else
		return day;
}
int day_of_forever(const int year, const int month, const int day)
{
	if (year > 0)
		return day_of_year(year, month, day) + day_of_forever(year - 1, 12, 31);
	else
		return day_of_year(year, month, day);
}

//Returns day of the week based on day_of_forever() calculation
string day_of_week(const int year, const int month, const int day)
{
	const int year_day = day_of_forever(year, month, day);
	if (year_day % 7 == 0)
		return "Friday";
	else if (year_day % 7 == 1)
		return "Saturday";
	else if (year_day % 7 == 2)
		return "Sunday";
	else if (year_day % 7 == 3)
		return "Monday";
	else if (year_day % 7 == 4)
		return "Tuesday";
	else if (year_day % 7 == 5)
		return "Wednesday";
	else
		return "Thursday";
}

int am_pm(const int hour)
{
	if (hour > 12)
		return hour - 12;
	else if (hour == 0)
		return 12;
	else
		return hour;
}

double to_radians(double a)
{
	const double pi = acos(-1.0);
	return a * pi / 180;
}


void circle(const int x, const int y, const int radius, const int degrees)
{
	if (degrees < 360)
	{
		const int radians = to_radians(degrees);
		move_to(x, y);
		set_pen_width(5);
		set_pen_color(color::black);
		if (degrees % 30 == 0)
		{
			move_distance(radius - radius / 10);
			draw_distance(radius / 10);
		}
		else
		{
			move_distance(radius);
			draw_point();
		}

		turn_right_by_degrees(1);
		circle(x, y, radius, degrees + 1);
	}
}

void draw_hour(const int x, const int y, const int hour, const int radius)
{
	set_heading_degrees(0);
	move_to(x, y);
	turn_left_by_degrees(360 - (30 * hour));
	set_pen_width(5);
	draw_distance(radius / 3.5);
}

void draw_minute(const int x, const int y, const int minute, const int radius)
{
	set_heading_degrees(0);
	move_to(x, y);
	turn_left_by_degrees(360 - (6 * minute));
	set_pen_width(5);
	draw_distance(radius / 1.5);
}

void draw_second(const int x, const int y, const int second, const int radius)
{
	set_heading_degrees(0);
	move_to(x, y);
	turn_left_by_degrees(360.0 - (6 * second));
	set_pen_width(3);
	draw_distance(radius / 2.5);
}

string date_end(const int date)
{
	if (date == 1 || date % 10 == 1)
		return "st ";
	else if (date == 2 || date % 10 == 2)
		return "nd ";
	else if (date == 3 || date % 10 == 3)
		return "rd ";
	else
		return "th ";
}


void time(const int monroe_hour, const int minute)
{
	fill_rectangle(get_window_width() / 3.75, get_window_height() / 8.5 - 45.0, 250.0, 70.0, color::white);
	const int start_time_x = get_window_width() / 3.75;
	const int start_time_y = get_window_height() / 8.5;
	move_to(start_time_x, start_time_y);

	set_font_size(70);
	set_font_style(1);

	write_string(monroe_hour);
	write_string(":");
	if (minute < 10)
		write_string("0");
	write_string(minute);
}

void write_date(const int year, const int month, const int day, const string month_written)
{
	const string day_week = day_of_week(year, month, day);
	const double weekday_start_x = get_window_width() / 2.7;
	const double weekday_start_y = get_window_height() - get_window_height() / 7.5;
	const double date_start_x = get_window_width() / 5.0;
	const double date_start_y = weekday_start_y + 50;

	fill_rectangle(date_start_x, weekday_start_y - 28, 350.0, 90.0, color::white);
	move_to(weekday_start_x, weekday_start_y);
	set_font_size(45);
	write_string(day_week);

	move_to(get_window_width() / 4.5, weekday_start_y + 50);

	write_string(day);
	write_string(date_end(day));
	write_string(month_written);
	write_string(year);

}


string month_string(const int month)
{
	const string months[] = { "January ", "February ", "March ", "April ",
		"May ", "June ", "July ", "August ", "September ",
		"October ", "November ", "December " };
	return months[month - 1];
}
void main()
{
	const int x = 250; //midpoint of clock x coordinate
	const int y = 300; //midpoint of clock y coordinate
	const int radius = 175;
	window * win = make_window(500, 600);
	win->set_caption("A Clock");
	circle(x, y, radius, 0);
	int current_day = 0, current_min=0;
	while (true)
	{
		const int clock_time = get_clock_time();
		const int date = get_calendar_date();

		const int day = date % 100;
		const int month = date % 10000 / 100;
		const int year = date / 10000;

		const int second = clock_time % 100;
		const int minute = clock_time % 10000 / 100;
		const int hour = clock_time / 10000;

		const int monroe_hour = am_pm(hour);
		const string month_written = month_string(month);
		const string day_week = day_of_week(year, month, day);
		
		set_pen_color(color::black);
		draw_hour(x, y, monroe_hour, radius);
		draw_minute(x, y, minute, radius);
		draw_second(x, y, second, radius);
		wait(0.1); //needed so clock doesn't blink

		if (minute != current_min || day != current_day) //only write date/time when it changes
		{
			current_day = day;
			current_min = minute;
			set_pen_color(color::black);
			time(monroe_hour, minute);
			if (hour > 12)
				write_string(" p.m.");
			else
				write_string(" a.m.");

			write_date(year, month, day, month_written);
		}

		//Erase previous time
		set_pen_color(color::white);
		draw_second(x, y, second, radius);
		draw_minute(x, y, minute, radius);
		draw_hour(x, y, hour, radius);
	}
}